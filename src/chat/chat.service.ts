import { Injectable } from '@nestjs/common';

@Injectable()
export class ChatService {
  private messages: Record<string, { sender: string; room: string; message: string }[]> = {};

  saveMessage(room: string, message: { sender: string; room: string; message: string }) {
    if (!this.messages[room]) {
      this.messages[room] = [];
    }
    this.messages[room].push(message);
  }

  getRomMessages(room: string): { sender: string; room: string; message: string }[] {    
    return this.messages[room] || [];
  }

  myMessageHistory(sender: string): { sender: string; room: string; message: string }[] {
    const history: { sender: string; room: string; message: string }[] = [];

    for (const room in this.messages) {
      if (this.messages.hasOwnProperty(room)) {
        for (const messageObject of this.messages[room]) {
          if (messageObject.sender === sender) {
            history.push(messageObject);
          }
        }
      }
    }

    return history;
  }

}
