import { Controller, Get, Query } from '@nestjs/common';
import { ChatService } from './chat.service';

@Controller('chat')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Get('messages')
  getRoomMessages(@Query('room') room: string) {
    const messages = this.chatService.getRomMessages(room);
    return { messages };
  }

  @Get('message-history')
  getMessageHistory(@Query('sender') sender: string) {
    const messages = this.chatService.myMessageHistory(sender);
    return { messages };
  }
}
